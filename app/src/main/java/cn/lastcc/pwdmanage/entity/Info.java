package cn.lastcc.pwdmanage.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 主要用于序列化保存密码标签数据以及其它类型程序数据
 * 
 * @author Added by baidongyang 2013-6-23
 * @version 13.6.23.1
 *
 */
public class Info implements Serializable {

    private static final long serialVersionUID = -8109403129818843853L;

    private List<PwdTag> pwdTagList = new ArrayList<PwdTag>();
    private String fileVersion = "";
    private int versionCode = 0;
    private String versionName = "";
    private String userPassword = "";

    /**
     * 获取属性：已保存的 PwdTag 对象集合
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public List<PwdTag> getPwdTagList() {
        return pwdTagList;
    }

    /**
     * 设置属性：已保存的 PwdTag 对象集合
     * @param pwdTagList
     * @author Added by baidongyang 2013-6-23
     */
    public void setPwdTagList(List<PwdTag> pwdTagList) {
        this.pwdTagList = pwdTagList;
    }

    /**
     * 获取属性：当前文件版本
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public String getFileVersion() {
        return fileVersion;
    }

    /**
     * 设置属性：当前文件版本
     * @param fileVersion
     * @author Added by baidongyang 2013-6-23
     */
    public void setFileVersion(String fileVersion) {
        this.fileVersion = fileVersion;
    }

    /**
     * 获取属性：保存当前文件的程序版本，对应应用程序 versionCode 属性
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public int getVersionCode() {
        return versionCode;
    }

    /**
     * 设置属性：保存当前文件的程序版本，对应应用程序 versionCode 属性
     * @param versionCode
     * @author Added by baidongyang 2013-6-23
     */
    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    /**
     * 获取属性：保存当前文件的程序向用户展示的版本号，对应应用程序 versionName 属性
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public String getVersionName() {
        return versionName;
    }

    /**
     * 设置属性：保存当前文件的程序向用户展示的版本号，对应应用程序 versionName 属性
     * @param versionName
     * @author Added by baidongyang 2013-6-23
     */
    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    /**
     * 获取属性：用户密码
     * @return
     * @author Added by baidongyang 2013-6-23
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * 设置属性：用户密码
     * @param userPassword
     * @author Added by baidongyang 2013-6-23
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
