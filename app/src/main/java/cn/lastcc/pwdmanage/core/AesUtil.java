package cn.lastcc.pwdmanage.core;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES 加密解密工具类
 * 
 * @author Added by baidongyang 2013-6-23
 *
 */
public class AesUtil {

    private static final String ALGORITHM = "AES";

    /**
     * 加密
     * 
     * @param content 需要加密的内容
     * @param password  加密密码
     * @return
     * @see http://blog.csdn.net/hbcui1984/article/details/5201247
     */
    public static byte[] encrypt(String content, String password, byte[] salt) {
            try {
                SecretKeySpec key = getSecretKey(password, salt);
                Cipher cipher = Cipher.getInstance(ALGORITHM);// 创建密码器
                byte[] byteContent = content.getBytes("utf-8");
                cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
                byte[] result = cipher.doFinal(byteContent); // 加密
                return result;
            } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
            } catch (InvalidKeyException e) {
                    e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
            } catch (BadPaddingException e) {
                    e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        return null;
    }

    /**解密
     * @param content  待解密内容
     * @param password 解密密钥
     * @param salt 盐值
     * @return
     * @see http://blog.csdn.net/hbcui1984/article/details/5201247
     */
    public static byte[] decrypt(byte[] content, String password, byte[] salt) {
            try {
                    SecretKeySpec key = getSecretKey(password, salt);
                    Cipher cipher = Cipher.getInstance(ALGORITHM);// 创建密码器
                    cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
                    byte[] result = cipher.doFinal(content); // 解密
                    return result;
            } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
            } catch (InvalidKeyException e) {
                    e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
            } catch (BadPaddingException e) {
                    e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        return null;
    }

    /**
     * 将二进制转换成16进制
     * @param buf
     * @return
     * @see http://blog.csdn.net/hbcui1984/article/details/5201247
     */
    public static String parseByte2HexStr(byte buf[]) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < buf.length; i++) {
                    String hex = Integer.toHexString(buf[i] & 0xFF);
                    if (hex.length() == 1) {
                            hex = '0' + hex;
                    }
                    sb.append(hex.toUpperCase());
            }
            return sb.toString();
    }

    /**
     * 将16进制转换为二进制
     * @param hexStr
     * @return
     * @see http://blog.csdn.net/hbcui1984/article/details/5201247
     */
    public static byte[] parseHexStr2Byte(String hexStr) {
            if (hexStr.length() < 1)
                    return null;
            byte[] result = new byte[hexStr.length()/2];
            for (int i = 0;i< hexStr.length()/2; i++) {
                    int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);
                    int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);
                    result[i] = (byte) (high * 16 + low);
            }
            return result;
    }

    /**
     * 生成 Salt
     * @param salt
     */
    public static void generateSalt(byte[] salt) {
        SecureRandom sr = new SecureRandom();
        sr.setSeed(System.currentTimeMillis());
        sr.nextBytes(salt);
    }

    /**
     * 生成密钥
     * @param password
     * @param salt
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    private static SecretKeySpec getSecretKey(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // AES 支持 128、192 和 256bit 长度的密钥。
        int keyLength = 256;
        // 生成 PBE 密钥
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 1000, keyLength);
        // 通过工厂获取密钥
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] key = keyFactory.generateSecret(keySpec).getEncoded();
        return new SecretKeySpec(key, ALGORITHM);
    }
}
